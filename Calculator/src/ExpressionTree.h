#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
using namespace std;

/** class TreeNode **/
class TreeNode
{
    public:
        char data;
        TreeNode *left, *right;
        /** constructor **/
        TreeNode(char data)
        {
            this->data = data;
            this->left = NULL;
            this->right = NULL;
        }
};

/** class StackNode **/
class StackNode
{
    public:
        TreeNode *treeNode;
        StackNode *next;
        /** constructor **/
        StackNode(TreeNode *treeNode)
        {
            this->treeNode = treeNode;
            next = NULL;
        }
};


class ExpressionTree
{
    private:
        StackNode *top;
    public:
        /** constructor **/
        ExpressionTree()
        {
            top = NULL;
        }

        /** function to push a node **/
        void push(TreeNode *tn)
        {
            if (top == NULL)
                top = new StackNode(tn);
            else
            {
                StackNode *sn = new StackNode(tn);
                sn->next = top;
                top = sn;
            }
        }

        /** function to pop a node **/
        TreeNode *pop()
        {
            if (top == NULL)
            {
                cout<<"not found"<<endl;
            }
            else
            {
                TreeNode *tn = top->treeNode;
                top = top->next;
                return tn;
            }
        }


        /** function to insert character **/
        void insert(char val)
        {
            if (isDigit(val))
            {
                TreeNode *nptr = new TreeNode(val);
                push(nptr);
            }
            else if (isOperator(val))
            {
                TreeNode *nptr = new TreeNode(val);
                nptr->left = pop();
                nptr->right = pop();
                push(nptr);
            }
        }

        /** function to check if digit **/
        bool isDigit(char ch)
        {
            return ch >= '0' && ch <= '9';
        }

        /** function to check if operator **/
        bool isOperator(char ch)
        {
            return ch == '+' || ch == '-' || ch == '*' || ch == '/';
        }

        /** function to build tree from input */
        void buildTree(string Expression)
        {
            for (int i = Expression.length() - 1; i >= 0; i--)
                insert(Expression[i]);
        }

