#include <SDL2/SDL.h>
#define n 3
SDL_Window *window = nullptr;
SDL_Renderer* renderer= nullptr;

int CenterX=683,CenterY=384,echellex=40,echelley=40;
double function (double x){
    return(x*x);}


void axes(void){
 int i;
 for(i=-470;i<470;i++){
	 SDL_SetRenderDrawColor(renderer,0,0,0,100);
	 SDL_RenderDrawPoint(renderer,CenterX+i,CenterY);

	 }
 for(i=-370;i<370;i++) {
	 SDL_SetRenderDrawColor(renderer,0,0,0,100);
	 SDL_RenderDrawPoint(renderer,CenterX,CenterY+i);
	 }

 for(i=-10;i<=10;i++) {
 	 SDL_SetRenderDrawColor(renderer,0,0,0,100);
 	 SDL_RenderDrawPoint(renderer,CenterX+echellex,CenterY+i);
 	 }
 for(i=-10;i<=10;i++) {
 	 SDL_SetRenderDrawColor(renderer,0,0,0,100);
 	 SDL_RenderDrawPoint(renderer,CenterX+i,CenterY+echelley);
 	 }
}
void drawFunction(void){
 double x,res;
 int pixelX,pixelY;
 	 for(x=-5.;x<5.;x+=0.001){
		res=function(x);
		pixelX=CenterX+(int)(echellex*x);
		pixelY=CenterY-(int)(echelley*res); //pixhel index are int not double
		if (pixelY>20 && pixelY<700){
		 SDL_SetRenderDrawColor(renderer,125,0,0,255);
		 SDL_RenderDrawPoint(renderer,pixelX,pixelY);}
 	 }
}


int main(int argc, char** argv){

    // Start SDL2
    SDL_Init(SDL_INIT_VIDEO);
    // Create a Window in the middle of the screen
	window = SDL_CreateWindow("Hello World!",
			SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, // x and y
			1366,768, // Width and Height
			SDL_WINDOW_OPENGL);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
    bool running = true;
    SDL_Event event;
    while(running){
		while(SDL_PollEvent(&event)){
			 if(event.type==SDL_QUIT){
				 running=false;
			 }
		}

		//set screen color to white
		SDL_SetRenderDrawColor(renderer,255,255,255,255);
		SDL_RenderClear(renderer);

		//Draw
		axes();
		drawFunction();
		//show what was drawn
		SDL_RenderPresent(renderer);

    }


    // Cleanup and Quit
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
